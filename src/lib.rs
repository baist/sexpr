use std::fmt;

const LST_OPN: char = '[';
const LST_CLS: char = ']';
const ESCP: char = '`';

#[derive(Debug)]
pub struct Error
{
    pub msg: &'static str,
}

#[derive(Debug)]
pub enum SExp {
    Atom(String),
    List(Vec<SExp>),
}

impl SExp {
    pub fn from_strings(v: &[&str]) -> Self
    {
        let l: Vec<SExp> = v.iter().map( |&x| x.into() ).collect();
        SExp::List(l)
    }

    pub fn as_atom(&self) -> &str
    {
        match *self {
            SExp::Atom(ref a) => {
                a.as_str()
            }
            SExp::List(_) => {
                ""
            }
        }
    }

    pub fn as_list(&self) -> &[SExp]
    {
        match *self {
            SExp::Atom(_) => {
                &[]
            }
            SExp::List(ref xs) => {
                xs.as_slice()
            }
        }
    }

    pub fn append(&mut self, s: SExp) {
        match *self {
            SExp::Atom(_) => {  }
            SExp::List(ref mut xs) => {
                xs.push(s);
            }
        }
    }
}

impl From<&str> for SExp {
    fn from(s: &str) -> Self {
        SExp::Atom(s.to_string())
    }
}

impl fmt::Display for SExp
{
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error>
    {
        match *self {
            SExp::Atom(ref a) => {
                // TODO: make separate function for this
                let s = a.replace("`", "``").replace("[", "`[").
                    replace("]", "`]").replace(" ", "` ");
                write!(f, "{}", s)
            }
            SExp::List(ref xs) => {
                if f.alternate() {
                    writeln!(f, "{}", LST_OPN)?;
                    for (_i, x) in xs.iter().enumerate() {
                        writeln!(f, "{:#}", x)?;
                    }
                    writeln!(f, "{}", LST_CLS)
                }
                else {
                    write!(f, "{}", LST_OPN)?;
                    for (i, x) in xs.iter().enumerate() {
                        let s = if i == 0 { "" } else { " " };
                        write!(f, "{}{}", s, x)?;
                    }
                    write!(f, "{}", LST_CLS)
                }
            }
        }
    }
}

pub struct Parser<'a>
{
    txt: &'a str,
    pos: usize
}

impl<'a> Parser<'a>
{
    pub fn new(txt: &'a str) -> Self
    {
        Self {
            txt,
            pos: 0
        }
    }

    pub fn parse(mut self) -> Result<SExp, Error>
    {
        self.skip_space();
        let mut res = match self.peek() {
            Some(LST_OPN) => {  Ok(SExp::List(self.parse_list()?))  }
            Some(_) => {  Err(Error{ msg: "unexpected symbol" })  }
            None => {  Err(Error{ msg: "empty" })  }
        };
        let s = res?;
        self.skip_space();
        res = match self.peek() {
            Some(_) => {  Err(Error{ msg: "not single root" }) }
            None => {  Ok(s)  }
        };
        res
    }

    fn skip_space(&mut self)
    {
        while let Some(ch) = self.peek() {
            if ch.is_ascii_whitespace() {  self.next();  } else {  break;  }
        }
    }

    fn parse_atom(&mut self) -> Result<String, Error>
    {
        // skip_space is not needed
        let mut cs: String = String::new();
        while let Some(ch) = self.peek() {
            if ch.is_ascii_whitespace() || ch == LST_OPN || ch == LST_CLS {
                break;
            }
            else if ch == ESCP {
                // parse escape seq
                self.next();
                if let Some(c) = self.peek() {
                    match c {
                        ESCP | LST_OPN | LST_CLS |
                        ' ' | '\n' | '\t' => {
                            cs.push(c);
                        }
                        _ => {
                            return Err(Error{ msg: "wrong escape symbol" });
                        }
                    }
                }
                else {
                    return Err(Error{ msg: "  `" });
                }
            }
            else {
                cs.push(ch);
            }
            self.next();
        }
        Ok(cs)
    }

    fn parse_list(&mut self) -> Result<Vec<SExp>, Error>
    {
        self.eat(LST_OPN)?;
        let mut sexps: Vec<SExp> = Vec::new();
        loop {
            self.skip_space();
            match self.peek() {
                Some(LST_CLS) => {  break;  }
                Some(_) => {  sexps.push(self.parse_sexp()?);  }
                None => {  break;  }
            }
        }
        self.eat(LST_CLS)?;
        Ok(sexps)
    }

    fn parse_sexp(&mut self) -> Result<SExp, Error>
    {
        self.skip_space();
        match self.peek() {
            Some(LST_OPN) => {
                Ok(SExp::List(self.parse_list()?))
            }
            Some(_) => {
                Ok(SExp::Atom(self.parse_atom()?))
            }
            None => {
                Ok(SExp::List(Vec::new()))
            }
        }
    }

    fn eat(&mut self, ch: char) -> Result<(), Error>
    {
        if Some(ch) == self.peek() {
            self.next();
            return Ok(());
        }
        Err(Error{ msg: "expected symbol" })
    }

      //////   ///////
     //////   ///////
    //////   ///////

    fn peek(&mut self) -> Option<char>
    {
        self.txt[self.pos..].chars().next()
    }

    fn next(&mut self)
    {
        if let Some(c) = self.peek() {
            let new_pos = self.pos + c.len_utf8();
            self.pos = new_pos;
        }
    }
}

#[test]
fn from_strings()
{
    let s = ["`", "aaaa bbb", "ccc", "!"];
    let r = "[`` aaaa` bbb ccc ! end`]]";
    let mut sexpr = SExp::from_strings(&s);
    sexpr.append("end]".into());
    assert_eq!(r, sexpr.to_string());
    assert_eq!(sexpr.as_atom(), "");
    let list = sexpr.as_list();
    assert_eq!(list[0].as_atom(), "`");
    assert_eq!(list[1].as_atom(), "aaaa bbb");
    assert_eq!(list[2].as_atom(), "ccc");
    assert_eq!(list[3].as_atom(), "!");
    assert_eq!(list[4].as_atom(), "end]");
}

#[test]
fn parse_void()
{
    let s = "";
    Parser::new(&s).parse().unwrap_err();
}

#[test]
fn parse_empty()
{
    let s = "[]";
    assert_eq!("[]", Parser::new(&s).parse().unwrap().to_string());
}

#[test]
fn parse_regular()
{
    let s = "  [ test    [aaa bbb]  ccc ]  ";
    let r = "[test [aaa bbb] ccc]";
    assert_eq!(r, Parser::new(&s).parse().unwrap().to_string());
}

#[test]
fn parse_atoms()
{
    let s = "[   this    is the list  of  atoms ]";
    let r = "[this is the list of atoms]";
    assert_eq!(r, Parser::new(&s).parse().unwrap().to_string());
}

#[test]
fn parse_grave()
{
    let s = "[``]";
    let r = "[``]";
    assert_eq!(r, Parser::new(&s).parse().unwrap().to_string());
}

#[test]
fn parse_extra_br()
{
    let s = "]";
    Parser::new(&s).parse().unwrap_err();
}

#[test]
fn parse_wrong_escape()
{
    let s = "`#";
    Parser::new(&s).parse().unwrap_err();
}

#[test]
fn parse_not_single_root()
{
    let s = "[][]";
    Parser::new(&s).parse().unwrap_err();
}

#[test]
fn parse_escapes()
{
    let s = "[`[name` with``` and` `]]";
    let r = "[`[name` with``` and` `]]";
    assert_eq!(r, Parser::new(&s).parse().unwrap().to_string());
}
