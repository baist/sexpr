# sexpr

The lightweight library for working with a specific hierarchical data format.

Very simple primitive data format is presented as with S-expressions like in programming language LISP.
More simpler than JSON and XML.

It consists of two types of objects: atom and lists.
Objects are separated by space and other white symbols(CR,LF,FF,HT).
Atom is a single value represented by a sequence of any characters of any length(exclude seperators).
List is defined using the opening and closing square bracket characters.

Symbol '\`' is escape character for symbols '[' ']' '\`' and whitespaces.

## example
```
[
  [name John` Doe]
  [age 30]
  [email john.doe@example.com]
  [isEmployed true]
  [address
    [street 123` Main` St]
    [city New-York]
    [zipcode 80010]
  ]
  [hobbies reading traveling cooking]
  [friends 
    [
        [name Alice]
        [age 28]
    ]
    [
        [name Bob]
        [age 32]
    ]
  ]
]
```
